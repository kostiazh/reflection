﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudWorks.Store.Attributes
{
    /// <summary>
    /// Attribute that indicate length of the string property.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class StringLengthAttribute : Attribute
    {
        /// <summary>
        /// Constructor of <see cref="StringLengthAttribute"/>.
        /// </summary>
        /// <param name="length">Max length of the string</param>
        /// <exception cref="ArgumentOutOfRangeException">occurs if length less or equal 0.</exception>
        public StringLengthAttribute(int length)
        {
            if (length <= 0)
                throw new ArgumentOutOfRangeException();
            
            this.Length = length;
        }
        
        /// <summary>
        /// Maximun length.
        /// </summary>
        public int Length { get; }
    }
}
